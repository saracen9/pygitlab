import logging
import gitlab
import os
import yaml

PERSONAL_ACCESS_TOKEN = os.environ.get('ACCESS_TOKEN')
REPO_NAME = os.environ.get('REPO_NAME')
GITLAB_USER = os.environ.get('GITLAB_USER')
LOG_LEVEL = os.environ.get('LOG_LEVEL', 'INFO')
PAGES_DOMAIN = os.environ.get('PAGES_DOMAIN', 'gitlab.io')


def __configure_logger(logger: logging.Logger):
    # Configure Logger
    logger.setLevel(
        logging.__dict__[LOG_LEVEL])
    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)
    # create formatter
    formatter = logging.Formatter(
        '%(asctime)s - %(threadName)s - %(levelname)s - %(message)s')
    # add formatter to ch
    ch.setFormatter(formatter)
    # add ch to logger
    logger.addHandler(ch)


def __authenticate(log: logging.Logger, token=PERSONAL_ACCESS_TOKEN) \
        -> gitlab.Gitlab:
    gl = gitlab.Gitlab('https://gitlab.com', private_token=token)
    gl.auth()
    return gl


def __setup_project(log: logging.Logger, gl: gitlab.Gitlab,
                    repo_name=REPO_NAME):
    # Get a project by userspace/name
    try:
        project = gl.projects.get(repo_name)
        log.info(f"Project found at {repo_name}")
    except Exception as e:
        if e.response_code == 404:
            log.info(f"{repo_name} not found. Creating new Project.")
            # No project found so create one
            # check if it's in a team or userspace
            repo_name = repo_name.split('/')
            group = GITLAB_USER != repo_name[0]
            # Create a project
            if not group:
                log.info(f"Project being created in user namespace.")
                project = gl.projects.create({'name': repo_name[1]})
            else:
                log.info(f"Project in group {repo_name[0]}")
                try:
                    log.info(f"Group {repo_name[0]} found.")
                    group = gl.groups.get(repo_name[0])
                except Exception as e:
                    if e.response_code == 404:
                        log.info(f"{repo_name[0]} not found. Creating Group.")
                        group = gl.groups.create({'name': repo_name[0],
                                                  'path': repo_name[0]})
                    else:
                        log.error("Unexpected error occurred")
                        log.error(e)
                        raise
                log.info(f"Project being created in group namespace.")
                project = gl.projects.create({'name': REPO_NAME[1],
                                              'namespace_id': group.id})
        else:
            log.error("Unexpected error occurred")
            log.error(e)
            raise
    return project


def __configure_labels(log: logging.Logger, project,
                       delete_existing=True):
    # Configure Labels
    # TODO: allow local settings to override and remove hardcoding
    log.info("Applying label preferences.")

    labels = project.labels.list()
    if len(labels) > 0:
        log.info("Existing labels found. Removing.")
        for l in labels:
            l.delete()

    path = os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'labels.yaml'
    )
    with open(path, 'r') as f:
        labels = yaml.load(f.read(), Loader=yaml.FullLoader)

    for cat, labels in labels.get('labels').items():
        if labels:
            for l in labels:
                name = f"[{cat.title()}]: {l}"
                color = labels[l]
                log.info(f"{name} ({color})")
                project.labels.create({'name': name, 'color': color})


def __setup_pages():
    # # Setup the pages
    # # https://gitlab.com/help/user/project/pages/getting_started_part_one.md
    # domains = project.pagesdomains.list()
    # if len(domains) > 0:
    #     for d in domains:
    #         log.info(f"Found domain: {d}")
    # else:
    #     log.info("No domains found.")
    #     pages_address = f"{REPO_NAME}"
    #     domain = project.pagesdomains.create({'domain': })
    raise NotImplementedError


def initialise_project():
    log = logging.getLogger(__name__)
    __configure_logger(log)

    gl = __authenticate(log=log)
    project = __setup_project(log=log, gl=gl)
    __configure_labels(log=log, project=project)


if __name__ == '__main__':
    initialise_project()

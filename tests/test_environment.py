import pytest


class TestClass:
    def test_one(self):
        x = "this"
        assert "h" in x

    @pytest.mark.xfail
    def test_two(self):
        x = "hello"
        assert hasattr(x, "check")

    # TODO: Check `python --version` output
    def test_python(self):
        pass
